﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ApiAgenda.Migrations
{
    public partial class CriarTabelaAgenda : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterDatabase()
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "Agenda",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Usuario = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Cep = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Logradouro = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Numero = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Complemento = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Bairro = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Cidade = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Estado = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Pais = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Observacao = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    InicioPrevisto = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    FimPrevisto = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    InicioReal = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    FimReal = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    CriadoEm = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    ModificadoEm = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Agenda", x => x.Id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.InsertData(
                table: "Agenda",
                columns: new[] { "Id", "Bairro", "Cep", "Cidade", "Complemento", "CriadoEm", "Estado", "FimPrevisto", "FimReal", "InicioPrevisto", "InicioReal", "Logradouro", "ModificadoEm", "Numero", "Observacao", "Pais", "Usuario" },
                values: new object[] { 1L, "", "17650-000", "", "", new DateTime(2022, 1, 25, 20, 47, 8, 923, DateTimeKind.Local).AddTicks(7150), "", new DateTime(2022, 1, 25, 20, 47, 9, 298, DateTimeKind.Local).AddTicks(8185), null, new DateTime(2022, 1, 25, 20, 47, 9, 298, DateTimeKind.Local).AddTicks(8174), null, "", new DateTime(2022, 1, 25, 20, 47, 8, 923, DateTimeKind.Local).AddTicks(7150), "", "", "", "admin" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Agenda");
        }
    }
}
