﻿using ApiAgenda.Data;

namespace ApiAgenda.Services
{
    public interface IAgendaService
    {
        Task<AgendaVO> Create(AgendaVO vo);

        Task<List<AgendaVO>> FindAll();
        
        Task<AgendaVO> FindById(long id);

        Task<List<AgendaVO>> FindByUsuario(string user);
        
        Task<AgendaVO> Update(long id, AgendaVO vo);

        Task<bool> Delete(long id);
    }
}
