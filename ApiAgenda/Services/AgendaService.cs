﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ApiAgenda.Data;
using ApiAgenda.Models;
using ApiAgenda.Models.Context;

namespace ApiAgenda.Services
{
    public class AgendaService : IAgendaService
    {
        private readonly DataContext dc;
        private IMapper mp;

        public AgendaService(DataContext dc, IMapper mp)
        {
            this.dc = dc;
            this.mp = mp;
        }

        public async Task<AgendaVO> Create(AgendaVO vo)
        {
            Agenda agenda = mp.Map<Agenda>(vo);
            dc.Agenda.Add(agenda);
            await dc.SaveChangesAsync();

            return mp.Map<AgendaVO>(agenda);
        }

        public async Task<List<AgendaVO>> FindAll()
        {
            List<Agenda> agenda = await dc.Agenda.AsNoTracking().ToListAsync();

            return mp.Map<List<AgendaVO>>(agenda);
        }

        public async Task<AgendaVO> FindById(long id)
        {
            Agenda? agenda = await dc.Agenda.AsNoTracking()
                                           .Where(x => x.Id == id)
                                           .FirstOrDefaultAsync();

            return mp.Map<AgendaVO>(agenda);
        }

        public async Task<List<AgendaVO>> FindByUsuario(string user)
        {
            List<Agenda> agenda = await dc.Agenda.AsNoTracking()
                                           .Where(x => x.Usuario == user)
                                           .ToListAsync();

            return mp.Map<List<AgendaVO>>(agenda);
        }

        public async Task<AgendaVO> Update(long id, AgendaVO vo)
        {
            if (id == vo.Id && AgendaExiste(vo.Id))
            {
                Agenda agenda = mp.Map<Agenda>(vo);
                agenda.ModificadoEm = DateTime.Now;
                dc.Agenda.Update(agenda);
                await dc.SaveChangesAsync();

                return mp.Map<AgendaVO>(agenda);
            }
            else
            {
                return null;
            }
        }

        public async Task<bool> Delete(long id)
        {
            try
            {
                Agenda? agenda = await dc.Agenda.FindAsync(id);

                if (agenda == null)
                {
                    return false;
                }

                dc.Agenda.Remove(agenda);
                await dc.SaveChangesAsync();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private bool AgendaExiste(long id)
        {
            return dc.Agenda.Any(e => e.Id == id);
        }
    }
}
