﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using ApiAgenda.Data;
using ApiAgenda.Services;
using ApiAgenda.Utils;

namespace ApiAgenda.Controllers
{
    [Route("api/v1")]
    [ApiController]
    [Authorize]
    public class AgendaController : ControllerBase
    {
        private IAgendaService agendaService;

        public AgendaController(IAgendaService agenda)
        {
            this.agendaService = agenda ?? throw new ArgumentNullException(nameof(agendaService));
        }

        [HttpPost]
        public async Task<ActionResult<AgendaVO>> Create([FromBody] AgendaVO vo)
        {
            if (vo == null) return BadRequest();

            var agenda = await agendaService.Create(vo);
            
            return Ok(agenda);
        }

        [HttpGet]
        public async Task<ActionResult<List<AgendaVO>>> FindAll()
        {
            var usuario = User.Identity.Name;
            List<AgendaVO> agenda;

            //if (User.IsInRole("Admin"))
            //    agenda = await agendaService.FindAll();
            //else
            agenda = await agendaService.FindByUsuario(usuario);

            return Ok(agenda);
        }

        [HttpGet("{id:long}")]
        public async Task<ActionResult<AgendaVO>> FindById(long id)
        {
            var agenda = await agendaService.FindById(id);

            if (agenda == null)
            {
                return NotFound();
            }

            return Ok(agenda);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<AgendaVO>> Update(long id, [FromBody] AgendaVO vo)
        {
            if (vo == null || id != vo.Id) return BadRequest();

            var agenda = await agendaService.Update(id, vo);

            return Ok(agenda);
        }

        
        [HttpDelete("{id}")]
        [Authorize(Roles = Role.Admin)]
        public async Task<IActionResult> DeleteAgenda(long id)
        {
            var status = await agendaService.Delete(id);
            
            if (!status) return BadRequest();
            
            return Ok(status);
        }
    }
}
