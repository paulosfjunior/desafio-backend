﻿using AutoMapper;
using ApiAgenda.Models;
using ApiAgenda.Data;

namespace ApiAgenda.Config
{
    public class MappingConfig
    {
        public static MapperConfiguration RegisterMaps()
        {
            var mappingConfig = new MapperConfiguration(config => {
                config.CreateMap<AgendaVO, Agenda>();
                config.CreateMap<Agenda, AgendaVO>();
            });
            return mappingConfig;
        }
    }
}