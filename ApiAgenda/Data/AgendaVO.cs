﻿namespace ApiAgenda.Data
{
    public class AgendaVO
    {
        public long Id { get; set; }

        public string Usuario { get; set; }

        public string Cep { get; set; }

        public string? Logradouro { get; set; }

        public string? Numero { get; set; }

        public string? Complemento { get; set; }

        public string? Bairro { get; set; }

        public string? Cidade { get; set; }

        public string? Estado { get; set; }

        public string? Pais { get; set; }

        public string? Observacao { get; set; }

        public DateTime InicioPrevisto { get; set; }

        public DateTime FimPrevisto { get; set; }

        public DateTime? InicioReal { get; set; }

        public DateTime? FimReal { get; set; }

        public DateTime CriadoEm { get; set; }

        public DateTime ModificadoEm { get; set; }
    }
}
