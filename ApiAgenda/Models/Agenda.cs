﻿using System.ComponentModel.DataAnnotations;

namespace ApiAgenda.Models
{
    public class Agenda
    {
        private static DateTime DataAtual = DateTime.Now;

        [Key]
        public long Id { get; set; }

        [Required(ErrorMessage = "Este campo é obrigatório")]
        public string Usuario { get; set; }

        [Required(ErrorMessage = "Este campo é obrigatório")]
        public string Cep { get; set; }

        public string? Logradouro { get; set; } = string.Empty;

        public string? Numero { get; set; } = string.Empty;

        public string? Complemento { get; set; } = string.Empty;

        public string? Bairro { get; set; } = string.Empty;

        public string? Cidade { get; set; } = string.Empty;

        public string? Estado { get; set; } = string.Empty;

        public string? Pais { get; set; } = string.Empty;

        public string? Observacao { get; set; } = string.Empty;

        [Required(ErrorMessage = "Este campo é obrigatório")]
        public DateTime InicioPrevisto { get; set; }

        [Required(ErrorMessage = "Este campo é obrigatório")]
        public DateTime FimPrevisto { get; set; }

        public DateTime? InicioReal { get; set; } = null;

        public DateTime? FimReal { get; set; } = null;

        public DateTime CriadoEm { get; set; } = DataAtual;

        public DateTime ModificadoEm { get; set; } = DataAtual;
    }
}