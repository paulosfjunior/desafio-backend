﻿using Microsoft.EntityFrameworkCore;

namespace ApiAgenda.Models.Context
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }

        public DbSet<Agenda> Agenda { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Agenda>().HasData(new Models.Agenda
            {
                Id = 1,
                Usuario = "admin",
                Cep = "17650-000",
                InicioPrevisto = DateTime.Now,
                FimPrevisto = DateTime.Now
            });
        }
    }
}