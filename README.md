# Desafio Backend

Desafio: Implementar um sistema simples de agendamento de Técnicos.


Um técnico poderá agendar uma visita técnica em uma determinada fazenda para realizar um serviço.


Regras

-	O sistema precisa ter autenticação

-	O técnico pode ser cadastrado diretamente via banco (para facilitar e reduzir horas de desenvolvimento)

-	O técnico tem que acessar o sistema e realizar um CRUD de agendamento

-	Colocar um campo “local” onde o técnico digita o cep do local, busca as informações como estado, cidade e bairro e preenche automático esses campos (API de cep referencia: https://viacep.com.br/)

-	Fazer documentação da API


Regras de agendamento:

-	O técnico poderá criar um agendamento;

-	O técnico poderá excluir um agendamento;

-	O técnico poderá editar o agendamento;

-	O técnico poderá ver somente seus agendamentos;

-	O técnico não poderá cadastrar uma visita anterior a data atual.

-	Em um agendamento o técnico poderá escrever detalhes de como foi a visita.

-	No agendamento deverá marcar data/hora de início e a data/hora final estimada pelo Técnico e poderá terminar a visita apenas antes da hora final estimada.
-	Na listagem deverá mostrar também, a hora final estimada e a hora que o técnico terminou a visita.


Os dados da visita deverão ser armazenados em um banco de dados local.


O que enviar:

Gravar um vídeo explicando o sistema rodando, seu funcionamento, conceitos, mostrar o banco de dados e os dados.


Utilizar tecnologias: ASPNET CORE, Banco de dados MySQL ou PostgreSQL Diferencial:

-	Desenvolver uma melhoria/funcionalidade nova nesse sistema para mostrar conceitos;

-	Multi-idioma.

-	Gerar documentação automática da API


Habilidades que serão avaliadas: .NET CORE, melhores práticas de programação, arquitetura e organização do código.
