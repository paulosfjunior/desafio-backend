﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Agendamento.Models;
using Agendamento.Services.IServices;
using Agendamento.Utils;
using Microsoft.AspNetCore.Authentication;

namespace Agendamento.Controllers
{
    [Route("Agenda")]
    public class AgendaController : Controller
    {
        private readonly IAgendaService agendaService;

        public AgendaController(IAgendaService agendaService)
        {
            this.agendaService = agendaService ?? throw new ArgumentNullException(nameof(agendaService));
        }

        [Authorize]
        [HttpGet("AgendaIndex")]
        public async Task<ActionResult> AgendaIndex()
        {
            var token = await HttpContext.GetTokenAsync("access_token");
            var agenda = await agendaService.FindAllAgendas(token);

            return View(agenda);
        }

        [HttpGet("AgendaCreate")]
        public async Task<ActionResult> AgendaCreate()
        {
            return View();
        }

        [Authorize]
        [HttpPost("AgendaCreate")]
        public async Task<ActionResult> AgendaCreate(AgendaModel model)
        {
            var DataAtual = DateTime.Now;

            if (model.InicioPrevisto >= DataAtual && model.FimPrevisto >= DataAtual && model.InicioPrevisto <= model.FimPrevisto && (model.FimReal != null ? model.FimReal < model.FimPrevisto : true))
            {
                if (ModelState.IsValid)
                {
                    model.Usuario = User.Identity.Name;
                    var token = await HttpContext.GetTokenAsync("access_token");
                    var response = await agendaService.CreateAgenda(model, token);

                    if (response != null) return RedirectToAction(nameof(AgendaIndex));
                }
            }

            return View(model);
        }

        [HttpGet("AgendaUpdate/{id:long}")]
        public async Task<IActionResult> AgendaUpdate(int id)
        {
            var token = await HttpContext.GetTokenAsync("access_token");
            var model = await agendaService.FindAgendaById(id, token);
            if (model != null) return View(model);
            return NotFound();
        }

        [Authorize]
        [HttpPost("AgendaUpdate/{id:long}")]
        public async Task<IActionResult> AgendaUpdate(long id, AgendaModel model)
        {
            var DataAtual = DateTime.Now;

            if (model.InicioPrevisto >= DataAtual && model.FimPrevisto >= DataAtual && model.InicioPrevisto <= model.FimPrevisto && (model.FimReal != null ? model.FimReal < model.FimPrevisto : true))
            {
                if (ModelState.IsValid)
                {
                    model.Usuario = User.Identity.Name;
                    var token = await HttpContext.GetTokenAsync("access_token");
                    var response = await agendaService.UpdateAgenda(id, model, token);
                    if (response != null) return RedirectToAction(nameof(AgendaIndex));
                }
            }

            return View(model);
        }

        [Authorize]
        [HttpGet("AgendaDelete")]
        public async Task<ActionResult> AgendaDelete(long id)
        {
            var token = await HttpContext.GetTokenAsync("access_token");
            var model = await agendaService.FindAgendaById(id, token);

            if (model != null) return View(model);

            return NotFound();
        }

        [Authorize(Roles = Role.Admin)]
        [HttpPost("AgendaDelete")]
        public async Task<ActionResult> AgendaDelete(AgendaModel model)
        {
            var token = await HttpContext.GetTokenAsync("access_token");
            var response = await agendaService.DeleteAgenda(model.Id, token);

            return RedirectToAction(nameof(AgendaIndex));
        }
    }
}
