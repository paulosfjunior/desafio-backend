﻿using Agendamento.Models;
using Agendamento.Services.IServices;
using Agendamento.Utils;
using System.Net.Http.Headers;

namespace Agendamento.Services
{
    public class AgendaService : IAgendaService
    {
        private readonly HttpClient client;
        public const string BasePath = "api/v1";

        public AgendaService(HttpClient client)
        {
            this.client = client;
        }

        public async Task<AgendaModel> CreateAgenda(AgendaModel model, string token)
        {
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await client.PostAsJson(BasePath, model);

            if (response.IsSuccessStatusCode)
                return await response.ReadContentAs<AgendaModel>();
            else throw new Exception("Algo deu errado ao chamar a API");
        }

        public async Task<List<AgendaModel>> FindAllAgendas(string token)
        {
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await client.GetAsync(BasePath);

            return await response.ReadContentAs<List<AgendaModel>>();
        }

        public async Task<AgendaModel> FindAgendaById(long id, string token)
        {
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await client.GetAsync($"{BasePath}/{id}");

            return await response.ReadContentAs<AgendaModel>();
        }

        public async Task<AgendaModel> UpdateAgenda(long id, AgendaModel model, string token)
        {
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await client.PutAsJson($"{BasePath}/{id}", model);

            if (response.IsSuccessStatusCode)
                return await response.ReadContentAs<AgendaModel>();
            else throw new Exception("Algo deu errado ao chamar a API");
        }

        public async Task<bool> DeleteAgenda(long id, string token)
        {
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await client.DeleteAsync($"{BasePath}/{id}");

            if (response.IsSuccessStatusCode)
                return await response.ReadContentAs<bool>();
            else throw new Exception("Algo deu errado ao chamar a API");
        }
    }
}
