﻿using Agendamento.Models;

namespace Agendamento.Services.IServices
{
    public interface IAgendaService
    {
        Task<AgendaModel> CreateAgenda(AgendaModel model, string token);

        Task<List<AgendaModel>> FindAllAgendas(string token);

        Task<AgendaModel> FindAgendaById(long id, string token);

        Task<AgendaModel> UpdateAgenda(long Id, AgendaModel model, string token);
        
        Task<bool> DeleteAgenda(long id, string token);
    }
}
